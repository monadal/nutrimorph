# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import brick.processing.dijkstra_1_to_n as dk_
from brick.component.glial_cmp import glial_cmp_t
from brick.component.extension import extension_t
from brick.component.soma import soma_t
from brick.general.type import array_t, site_h, site_path_h
import brick.processing.input as in_
from sklgraph.brick.edge import _ReOrderedSites

import itertools as it_
from typing import Callable, Sequence, Tuple
import time as tm_

import numpy as np_
import matplotlib.pyplot as pl_
import skimage.graph as gr_


def CandidateConnections(
    somas: Sequence[soma_t],
    influence_map: array_t,
    dist_to_closest: array_t,
    extensions: Sequence[extension_t],
    max_straight_sq_dist: float = np_.inf,
) -> list:
    '''
    Find the endpoints candidate for connection to a certain soma based on the influence map and the distance map.
    '''
    #
    candidate_conn_nfo = []  # conn=connection

    # Select only the unconnected extensions
    extensions = filter(lambda ext: ext.is_unconnected, extensions)

    # In the unconnected ext, find the appropriate candidates
    for soma, extension in it_.product(somas, extensions):
        # Find the endpoints of the extensions
        new_candidates = extension.EndPointsForSomaOrExt(soma.uid, influence_map)
        # Add the candidate endpoints if their euclidian distance to the soma is < to the maximum distance allowed
        candidate_conn_nfo.extend(
            (ep_idx, soma, extension, end_point)
            for ep_idx, end_point in enumerate(new_candidates)
            if dist_to_closest[end_point] <= max_straight_sq_dist
        )
    # Sort the candidate by closest distance to the soma
    candidate_conn_nfo.sort(key=lambda elm: dist_to_closest[elm[3]])

    return candidate_conn_nfo


def ShortestPathFromToN(
    image: array_t,
    point: site_h,
    extension: extension_t,
    costs: array_t,
    candidate_points_fct: Callable,
    all_end_points: list = None,
    extensions: tuple = None,
    max_straight_sq_dist: float = np_.inf,
    erode_path: bool = False,
) -> Tuple[site_path_h, float]:
    '''
    Find the shortest weighted path between endpoints and soma closest contour points.
    Return path, path length.
    Very fast technique, because not iterating for each point.
    Keeps into memory all the shortest path, as a wave propagating and build upon that.
    '''
    # Finds the soma contour points the closest to the extension
    # candidate_indexing : Tuple[Tuple(int,int,int),...]
    candidate_points, candidate_indexing = candidate_points_fct(point, max_straight_sq_dist)

    # Erode the candidate points of the extensions for Ext <-> Ext connexion
    if erode_path:
        if candidate_indexing is not None:
            # reformat to modify them
            candidate_points = list(candidate_points)
            candidate_indexing_var = list(candidate_indexing)
            candidate_indexing = []
            for idx in candidate_indexing_var:
                idx = list(idx)
                candidate_indexing.append(idx)

            # Delete the points that are not endpoints and that are less than 2 pixels away from the closest endpoint
            # -- important for dilatation/erosion process
            for candidate, indx in enumerate(candidate_points):
                # Verify if end point
                if candidate not in all_end_points:
                    # Here  necessary to delete the 2 closest points next to the end points
                    # because if the all extension is candidate there might be a endpoint
                    # that will be polluted by the connection
                    not_allowed = set()
                    for e_p in all_end_points:
                        not_allowed_ = set(
                            (e_p[0] + i, e_p[1] + j, e_p[2] + k)
                            for i in (-2, -1, 0, 1, 2)
                            for j in (-2, -1, 0, 1, 2)
                            for k in (-2, -1, 0, 1, 2)
                            if i != 0 or j != 0 or k != 0)
                        not_allowed |= not_allowed_

                    if candidate in not_allowed:
                        for i in range(3):
                            candidate_indexing[i].pop(indx)
                        candidate_points.remove(candidate)

            # reformatting
            candidate_points = tuple(candidate_points)
            candidate_indexing = tuple(candidate_indexing)

            # Erode the end_point of the extension AND the extension candidate points in the costs map
            Erode(candidate_points, costs, extension, extensions, image, all_end_points=all_end_points)

    # If no path, return empty tuple for path and infinite path length.
    if candidate_points is None:
        return (), np_.inf

    # Initialize the cost map at the point
    costs[point] = 0.0
    costs[candidate_indexing] = 0.0

    # Find the shortest weighted path and its length with Dijkstra algorithm
    path, length = dk_.DijkstraShortestPath(costs, point, candidate_points)

    # # Using skimage instead : /!\ BUT way too long because of iterations /!\
    # paths = []
    # for idx, candidates in enumerate(candidate_points):
    #     routes, cost = gr_.route_through_array(costs, point, candidates, fully_connected=True, geometric=True)
    #     paths.append((routes, cost))
    #
    # path = tuple(min(paths, key=lambda elm: elm[1]))[0]
    #
    # print("\npath: ", path)
    #
    # elapsed_time = tm_.gmtime(tm_.time() - start_time)
    # print(f"\nElapsed Time={tm_.strftime('%Hh %Mm %Ss', elapsed_time)}")
    #
    # if len(path) != 0:
    #     length = len(path) - 2
    # else:
    #     length = 0

    # Set the costs to infinite to avoid a path to go twice through a same voxel
    costs[point] = np_.inf
    costs[candidate_indexing] = np_.inf

    return path, length


def ValidateConnection(
    glial_cmp: glial_cmp_t, extension: glial_cmp_t, end_point: tuple, dijkstra_path: site_path_h, costs: array_t, all_ep: list,
) -> None:
    '''
    Keep the connection path in the glial_cmp.
    Add the extension to the extensions list of the glial_cmp.
    '''
    #
    # Format the connexion path
    connection_path = tuple(zip(*dijkstra_path[1:-1]))
    if connection_path.__len__() == 0:
        connection_path = None

    # Store the connexion path in the glial_cmp
    glial_cmp.connection_path[extension.uid] = connection_path
    # Add the connected extension to the list of the extensions of the glial_cmp
    glial_cmp.extensions.append(extension)
    # TODO BackReferenceSoma
    extension.BackReferenceSoma(glial_cmp)

    # Add the site of the connexion between the extension and the soma, for each soma and for each extension
    # restrain the verification to the soma <-> ext step

    # Store the new endpoints of the extended extension - TODO update the end point vector
    all_ep.remove(end_point)

    if type(glial_cmp) is soma_t:
        end_point = UpdateEndPointsWithConnexionPath(connection_path, end_point)
        glial_cmp.ext_roots.append((extension.uid, end_point))

    if connection_path is not None:
        costs[connection_path] = np_.inf

        # add the connexion sites to the extension sites
        ext_sites = np_.asarray(extension.sites)
        extension.sites = np_.zeros((len(ext_sites), len(ext_sites[0])+len(connection_path[0])), dtype=np_.int64)
        for i in range(3):
            sites = ext_sites[i].tolist()
            sites += list(connection_path[i])
            extension.sites[i] = np_.asarray(sites, dtype=np_.int64)
        # reformat
        extension.sites = tuple(extension.sites)



def UpdateEndPointsWithConnexionPath(connexion_path: tuple, end_point: tuple) -> tuple:
    #
    if connexion_path is None:
        return end_point

    else:
        # Search the connection point link to the extension
        connexion_path = tuple(zip(*connexion_path))

        close_end_pt = tuple(
            (end_point[0] + i, end_point[1] + j, end_point[2] + k)
            for i in (-1, 0, 1)
            for j in (-1, 0, 1)
            for k in (-1, 0, 1)
            if i != 0 or j != 0 or k != 0)

        if connexion_path[0] in close_end_pt:
            return connexion_path[-1]

        elif connexion_path[-1] in close_end_pt:
            return connexion_path[0]


def Dilate(path: tuple, costs_map: array_t, cost: float = np_.inf) -> None:
    '''
    Put to the value cost in the cost map the neighbors voxels of a given path.
    The path must be in the format: array([[x1,x2,...],[y1,y2,...],[z1,z2,...]])
    Return the cost map.
    '''
    for site in list(zip(*path)):
        dilated_ext = [
            (site[0] + i, site[1] + j, site[2] + k)
            for i in (-1, 0, 1)
            for j in (-1, 0, 1)
            for k in (-1, 0, 1)
            if i != 0 or j != 0 or k != 0]
        for ext in dilated_ext:
            shape = costs_map.shape
            if (ext > (0, 0, 0)) and (ext[0] < shape[0]) and (ext[1] < shape[1]) and (ext[2] < shape[2]):
                costs_map[int(ext[0]), int(ext[1]), int(ext[2])] = cost


def Erode(path: Tuple[tuple],
          costs_map: array_t,
          extension: extension_t,
          extensions: tuple,
          image: array_t,
          all_end_points=None,
          ) -> None:
    '''
    Erode the voxels of a given path or point, without eroding the neighboring extension pixels and their neighbors.
    The path must be in the format: Tuple(tuple(x1, y1, z1), ...)
    '''
    #
    new_path = []

    for point in path:
        # find out whether this is a end point or not
        if point in all_end_points:
            ErodeEndPoint(point, costs_map, extension, image)
        else:
            # build a new path with no endpoints inside
            new_path.append(point)

    if new_path.__len__() > 0:
        ErodePath(tuple(new_path), costs_map, extensions, image)


def ErodeEndPoint(end_point: tuple, costs_map: array_t, extension: extension_t, image: array_t) -> None:
    '''
    Erode the endpoint of the extension to be connected and its neighbors.
    '''
    #
    dilated_point = set(
        (end_point[0] + i, end_point[1] + j, end_point[2] + k)
        for i in (-1, 0, 1)
        for j in (-1, 0, 1)
        for k in (-1, 0, 1)
        if i != 0 or j != 0 or k != 0)

    for site in tuple(dilated_point):
        if site in zip(*extension.sites):
            dilated_site = set(
                (site[0] + i, site[1] + j, site[2] + k)
                for i in (-1, 0, 1)
                for j in (-1, 0, 1)
                for k in (-1, 0, 1)
                if i != 0 or j != 0 or k != 0)

            dilated_point.remove(site)
            dilated_point = dilated_point.difference(dilated_site)
            # Should only have one point in common
            break

    dilated_point = list(dilated_point)

    shape = costs_map.shape
    for ext in dilated_point:
        if (tuple(ext) > (0, 0, 0)) and (tuple(ext)[0] < shape[0]) and (tuple(ext)[1] < shape[1]) and (tuple(ext)[2] < shape[2]):
            costs_map[ext[0], ext[1], ext[2]] = 1.0 / (image[ext[0], ext[1], ext[2]] + 1.0)


def ErodePath(path: tuple, costs_map: array_t, extensions: tuple, image: array_t) -> None:
    '''
    Erode the voxels of a given path, without eroding the neighboring extension pixels and their neighbors.
    '''
    #
    dilated = set()
    for point in path:
        dilated_point = set(
            (point[0] + i, point[1] + j, point[2] + k)
            for i in (-1, 0, 1)
            for j in (-1, 0, 1)
            for k in (-1, 0, 1)
            if i != 0 or j != 0 or k != 0)

        # Union
        dilated |= dilated_point

    dilated -= set(zip(*path))

    for extension in extensions:
        for site in zip(*extension.sites):
            if (site in dilated) and (site not in path):
                dilated.remove(site)
                # # Here the work of the strict erosion is already done in ShortestPathFromToN.
                # # So we allow more voxels to be erode in this condition in ErodePath.
                # dilated_site = set(
                #     (site[0] + i, site[1] + j, site[2] + k)
                #     for i in (-1, 0, 1)
                #     for j in (-1, 0, 1)
                #     for k in (-1, 0, 1)
                #     if i != 0 or j != 0 or k != 0)
                # dilated = dilated.difference(dilated_site)

    dilated = list(dilated)

    shape = costs_map.shape
    for ext in dilated:
        if (tuple(ext) > (0, 0, 0)) and (tuple(ext)[0] < shape[0]) and (tuple(ext)[1] < shape[1]) and (
                tuple(ext)[2] < shape[2]):
            costs_map[ext[0], ext[1], ext[2]] = 1.0 / (image[ext[0], ext[1], ext[2]] + 1.0)