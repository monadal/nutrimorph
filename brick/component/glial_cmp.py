# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from __future__ import annotations

from brick.general.type import array_t, np_array_picker_h, py_array_picker_h, site_path_h

from abc import abstractmethod
from typing import Dict, List, Optional, Tuple


class glial_cmp_t:
    #
    # extensions: connected downstream (i.e. going away from the soma)
    #
    __slots__ = ("uid", "sites", "connection_path", "extensions", "img_shape")

    def __init__(self):
        #
        for slot in self.__class__.__slots__:
            setattr(self, slot, None)
        # self.uid  # type: Optional[int]
        # self.sites  # type: Optional[np_array_picker_h]
        # self.connection_path  # type: Optional[Dict[int, py_array_picker_h]]
        # self.extensions  # type: Optional[List[glial_cmp_t]]
        # self.img_shape  # type: Optional[Tuple[int, ...]]

    def InitializeFromMap(self, bmp: array_t, uid: int) -> None:
        '''
        Initialize uid, sites, connection_path, extensions and image_shape.
        '''
        #
        self.uid = uid
        # sites: might contain voxels that could be removed w/o breaking connectivity
        self.sites = bmp.nonzero()
        self.connection_path = {}
        self.extensions = []
        self.img_shape = bmp.shape

    @property
    def has_extensions(self) -> bool:
        #
        return self.extensions.__len__() > 0

    @abstractmethod
    def BackReferenceSoma(self, glial_cmp: glial_cmp_t) -> None:
        #
        pass
