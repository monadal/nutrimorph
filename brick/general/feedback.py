# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import brick.processing.plot as ot_
from brick.component.extension import extension_t
from brick.component.soma import soma_t


from typing import Sequence, Tuple

import matplotlib as mpl_
import matplotlib.pyplot as pl_
mpl_.use('TkAgg')


def PrintConnectedExtensions(extensions: Sequence[extension_t]) -> None:
    #
    ext_uids = tuple(
        extension.uid for extension in extensions if extension.soma_uid is not None
    )
    print(
        f"    Connected Ext = {ext_uids.__len__()}"
        f"/{extensions.__len__()}\n"
        f"    {ext_uids}"
    )


def PlotSomas(somas: Sequence[soma_t], som_nfo: dict, axes: dict) -> None:
    #
    for soma in somas:
        axes[soma.uid] = ot_.PlotLMap(som_nfo["lmp"], labels=soma.uid)
        pl_.title(f"Soma.{soma.uid}")
    pl_.matshow(som_nfo["influence_map"].max(axis=0)), pl_.title("Soma Influencess")
    # pl_.show(block=True)
    pl_.matshow(som_nfo["dist_to_closest"].max(axis=0)), pl_.title("Soma Distances")
    # pl_.show(block=True)


def PlotExtensions(
    extensions: Sequence[extension_t], ext_nfo: dict, img_shape: Tuple[int, ...]
) -> None:
    #
    for extension in extensions:
        _ = ot_.PlotExtensions(extension, img_shape)
        pl_.title(f"Extension.{extension.uid}")
    pl_.matshow(ext_nfo["map"].max(axis=0))
    pl_.title("Extensions Extremities")
    # pl_.show(block=True)


def PlotSomasWithExtensions(somas: Sequence[soma_t], som_nfo: dict, which: str) -> None:
    #
    any_plot = False
    for soma in filter(lambda elm: elm.has_extensions, somas):
        if which == "with_ext_of_ext":
            for extension in soma.extensions:
                if not extension.has_extensions:
                    continue
        _ = ot_.PlotSomaWithExtensions(soma, som_nfo["lmp"])
        pl_.title(f"Soma.{soma.uid} + Ext.{tuple(ext.uid for ext in soma.extensions)}")
        # pl_.show(block=True)
        any_plot = True

    if any_plot:
        pl_.matshow(som_nfo["soma_w_ext_lmp"].max(axis=0))
        pl_.title("Somas + Extensions")
        # pl_.show(block=True)
