# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import frangi3 as fg_

import sys as sy_
from typing import Callable

import matplotlib.pyplot as pl_
import matplotlib.widgets as wg_
import numpy as np_
import skimage.io as io_

x = sy_.argv[4]
y = sy_.argv[5]
channel = sy_.argv[6]

image = io_.imread(sy_.argv[1])[:, x, y, channel]
print("IMAGE SHAPE=", image.shape)

mip_axis = int(sy_.argv[2])
bright_on_dark = sy_.argv[3] == "True"


def _NewSlider(
    position: int,
    label: str,
    value_min: float,
    value_max: float,
    value_init: float,
    UpdateFrangi: Callable,
) -> wg_.Slider:
    #
    global figure

    slider_room = figure.add_subplot(figure.grid[position, :], label=label)
    output = wg_.Slider(
        slider_room, label, value_min, value_max, valinit=value_init, closedmin=False,
    )
    output.on_changed(UpdateFrangi)

    return output


def _UpdateComponentsForNewScale(_) -> None:
    #
    global figure

    scale = figure.frangi_scale_slider.val

    (
        figure.plate_map,
        figure.blob_map,
        figure.bckgnd,
        figure.eig_condition,
    ) = fg_.FrangiSingleScaleComponents(
        image, scale, fg_.FillHessianMatricesW2FirstOrders, bright_on_dark=bright_on_dark,
    )

    _UpdateEnhancementForNewParameter(None)


def _UpdateEnhancementForNewParameter(_) -> None:
    #
    alpha = figure.parameters_sliders[0].val
    beta = figure.parameters_sliders[1].val
    frangi_c = figure.parameters_sliders[2].val

    enhanced = fg_.FrangiSingleScaleCombination(
        figure.plate_map,
        figure.blob_map,
        figure.bckgnd,
        figure.eig_condition,
        alpha=alpha,
        beta=beta,
        frangi_c=frangi_c,
    )
    enhanced_2d = np_.max(enhanced, axis=mip_axis)

    image_pl = figure.main_axes.imshow(enhanced_2d, cmap="hot")
    if figure.previous_colorbar is not None:
        figure.previous_colorbar.remove()
    figure.previous_colorbar = figure.colorbar(image_pl, ax=figure.main_axes)


figure = pl_.figure()
figure.set_constrained_layout(True)
figure.set_constrained_layout_pads(hspace=0, wspace=0)
figure.grid = figure.add_gridspec(
    nrows=5, ncols=2, height_ratios=[0.025, 0.9, 0.025, 0.025, 0.025]
)

input_axes = figure.add_subplot(figure.grid[1, 0], label="input")
input_axes.xaxis.tick_top()
input_axes.set_ylabel("Row")
input_axes.format_coord = lambda x, y: f"R:{int(y + 0.5)},C:{int(x + 0.5)}"
input_axes.imshow(np_.max(image, axis=mip_axis), cmap="gray")

figure.main_axes = figure.add_subplot(figure.grid[1, 1], label="main")
figure.main_axes.xaxis.tick_top()
figure.main_axes.set_ylabel("Row")
figure.main_axes.format_coord = lambda x, y: f"R:{int(y + 0.5)},C:{int(x + 0.5)}"
figure.previous_colorbar = None

figure.frangi_scale_slider = _NewSlider(
    0, "Scale", 0.0, 5.0, 1.0, _UpdateComponentsForNewScale
)
figure.parameters_sliders = []
figure.parameters_sliders.append(
    _NewSlider(2, "Alpha", 0.0, 5.0, 0.5, _UpdateEnhancementForNewParameter)
)
figure.parameters_sliders.append(
    _NewSlider(3, "Beta", 0.0, 5.0, 0.5, _UpdateEnhancementForNewParameter)
)
figure.parameters_sliders.append(
    _NewSlider(4, "Frangi_c", 0.0, 1000, 500.0, _UpdateEnhancementForNewParameter)
)

_UpdateComponentsForNewScale(None)

pl_.show()
