# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from brick.general.type import array_t
import numpy as np_
import math as mt_
from PIL import Image
from PIL.ExifTags import TAGS
import re as re_


def ImageVerification(image: array_t, channel: str) -> array_t:
    '''
    Verification of the dimension of the image and of the coherence with the entered 'chanel' parameter.
    '''
    #
    # The image must not be constant
    if image.max() == image.min():
        raise ValueError('The input image should not be constant.')

    # Verification of the dimension of the image and its coherence with the parameters (channel)
    elif image.ndim == 3:
        print('1 CHANNEL')
        if channel is not None:
            print('Warning: The image has only 3 dimensions. Give the channel the value None in the parameters.')
        return image

    elif image.ndim == 4:
        if channel == 'R' or channel == 'G' or channel == 'B':
            # not changed into if --channel in 'RGB'-- because if channel='RG' => True.
            print('MULTIPLE CHANNELS: ', channel, ' specified in the parameters.')

            image = image[:, :, :, 'RGB'.find(channel)]

            # The obtained image must not be constant
            if image.max() == image.min():
                raise ValueError('The input image should not be constant. Verify the channel parameter.')

            return image

        else:
            raise ValueError('The image has multiple color channels. Error in the value of the parameter channel.')

    elif image.ndim != 4 and image.ndim != 3:
        raise ValueError(f'The image dimensions are not correct: {image.ndim}, instead of 3 or 4.')


def IntensityNormalizedImage(image: array_t) -> array_t:
    '''
    Relative normalization of the image between 0 and 1.
    No division per 0 since the image should not be constant (ImageVerification function).
    '''
    #
    # print('Relative Intensity Normalization between 0 and 1.')

    value_max = image.max()
    value_min = image.min()

    result = (image.astype(np_.float32) - value_min) / float(value_max - value_min)

    return result

    # print(
    #     "This normalization does not bring anything; left as is for now to avoid the need for changing prms"
    # )
    # nonextreme_values = image[np_.logical_and(image > 0.0, image < image.max())]
    #
    # if nonextreme_values.size > 0:
    #     nonextreme_avg = nonextreme_values.mean()
    #     result = image.astype(np_.float32) / nonextreme_avg
    # else:
    #     result = image.astype(np_.float32)
    #
    # return result


def FindVoxelDimensionInMicron(data_path: str, size_voxel_in_micron: list = None) -> array_t:
    '''
    Find Voxel dimension in micron from the image metadata.
    '''
    #
    if size_voxel_in_micron is not None:
        print('VOXEL DIM: [X Y Z] =', size_voxel_in_micron, 'micron.')
        return np_.array(size_voxel_in_micron)

    else:  # TODO if not found, try to make something more general
        print('Warning: The size of a voxel is not specified in the parameters.')

        try:
            # Find the voxels dimensions in micron in the metadata.
            # /!\ Very specific to one type of microscope metadata !

            with Image.open(data_path) as img:
                # Use the exif tags into the image metadata
                meta_dict = {TAGS.get(key, 'missing'): img.tag[key] for key in
                             img.tag}

            # Decode the tags text
            metadata = meta_dict['missing'].decode('utf8')
            metadata = metadata.replace('\x00', '')

            # Initialize the list of voxel size in str
            voxel_size = []

            for axe in 'XYZ':
                pattern = 'Voxel' + axe + '.+\= (\d.+E.\d.)'  # Regular expression found in metadata
                voxel_size.append(re_.findall(pattern, metadata)[0])

            voxel_size = np_.array(list(map(float, voxel_size)))
            # Conversion meters in micron
            voxel_size_micron = 1.0e6 * voxel_size

            print('VOXEL DIM: [X Y Z] =', voxel_size_micron, 'micron.')

            return voxel_size_micron

        except:
            raise ValueError('/!\ Unable to find the voxel dimensions in micron. Specify it in the parameters.')


def ToPixel(micron: float, voxel_size_micron: list, dimension: tuple = (0,), decimals: int = None) -> int:
    '''
    Dimension correspond to the axis (X,Y,Z) = (0,1,2). Can be used for distance, area and volumes.
    '''
    # Conversion of micron into pixels.
    return round(micron / (mt_.prod(voxel_size_micron[axis] for axis in dimension)), decimals)


def ToMicron(pixel: float, voxel_size_micron: list, dimension: tuple = (0,), decimals: int = None) -> float:
    '''
    Dimension correspond to the axis (X,Y,Z) = (0,1,2). Can be used for distance, area and volumes.
    '''
    # Conversion of pixels into microns
    return round(pixel * (mt_.prod(voxel_size_micron[axis] for axis in dimension)), decimals)


def DijkstraCosts(image: array_t, som_map: array_t, ext_map: array_t) -> array_t:
    '''
    Gives the value inf if the voxel belongs to a soma or an extension.
    Otherwise, gives the value 1 / (voxel intensity + 1).

    The closer to cost 1, the less probable a connexion will take this path.
    The closer to cost 0.5, the more probable.
    '''
    dijkstra_costs = 1.0 / (image + 1.0)
    dijkstra_costs[np_.logical_or(som_map > 0, ext_map > 0)] = np_.inf

    return dijkstra_costs
