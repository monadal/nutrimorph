# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

from brick.general.type import array_t

import numpy as np_


shifts_of_26_neighbors_c = tuple(
    (i, j, k)
    for i in (-1, 0, 1)
    for j in (-1, 0, 1)
    for k in (-1, 0, 1)
    if i != 0 or j != 0 or k != 0   # take every direct neighbors except itself
)
# Must be positive and higher (strictly) than the max number of neighbors
invalid_n_neighbors_c = shifts_of_26_neighbors_c.__len__() + 1


def PartLMap(map_: array_t) -> array_t:
    #
    """
    The part mask is labeled as follows: background=invalid_n_neighbors_c;
    Pixels of the skeleton = number of neighboring pixels that belong to the skeleton
    (as expected, isolated pixels receive 0).
    """
    #
    result = np_.array(map_, dtype=np_.int8)
    # Create a binary map with the elements strict. positive = 1
    result[result > 0] = 1
    # Pad for voxels at the border --> gives a map of booleans (binary mask)
    padded_sm = np_.pad(map_ > 0, 1, "constant")

    # Roll a mask on the 26 neighbors to find connectivity
    for shifts in shifts_of_26_neighbors_c:
        result += np_.roll(padded_sm, shifts, axis=(0, 1, 2))[1:-1, 1:-1, 1:-1]

    # Remaining pixels are invalid neighbors (=27 for the background), different from 0 (0 connectivity)
    result[map_ == 0] = invalid_n_neighbors_c + 1

    # return the number of neighbors for each voxel, the voxel itself being not included
    return result - 1
