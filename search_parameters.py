# Copyright CNRS/Inria/UNS
# Contributor(s): Eric Debreuve (since 2019), Morgane Nadal (2020)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import configparser
from typing import Union, Any

# --Parameters from .INI file
parameters = configparser.ConfigParser(allow_no_value=True)
parameters.read('parameters.ini')


def IfNotFloat(section: str, key: str) -> Union[Union[float, str], Any]:
    if parameters.get(section, key) is not None:
        try:
            value = parameters.getfloat(section, key)
            return value
        except:
            return eval(parameters.get(section, key))
    else:
        return parameters.get(section, key)


# [Input]
data_path = parameters['Input']['data_path']
channel = parameters['Input']['channel']
size_voxel_in_micron = IfNotFloat('Input', 'size_voxel_in_micron')
dilatation_erosion = parameters.getboolean('Input', 'dilatation_erosion')
save_images = parameters['Input']['save_images']
save_csv = parameters['Input']['save_csv']
save_features_analysis = parameters['Input']['save_features_analysis']
statistical_analysis = parameters.getboolean('Input', 'statistical_analysis')

# [Somas]
soma_low_c = IfNotFloat('Somas', 'soma_low_c')
soma_high_c = IfNotFloat('Somas', 'soma_high_c')
soma_selem_micron_c = IfNotFloat('Somas', 'soma_selem_micron_c')
soma_min_area_c = IfNotFloat('Somas', 'soma_min_area_c')
soma_max_area_c = IfNotFloat('Somas', 'soma_max_area_c')

# [Extensions]
adapt_hist_equalization = parameters.getboolean('Extensions', 'adapt_hist_equalization')
ext_low_c = IfNotFloat('Extensions', 'ext_low_c')
ext_high_c = IfNotFloat('Extensions', 'ext_high_c')
ext_selem_micron_c = IfNotFloat('Extensions', 'ext_selem_micron_c')
ext_min_area_c = IfNotFloat('Extensions', 'ext_min_area_c')

# [Connexions]
max_straight_sq_dist_c = IfNotFloat('Connexions', 'max_straight_sq_dist_c')
max_weighted_length_c = IfNotFloat('Connexions', 'max_weighted_length_c')

# [Frangi]

scale_range = IfNotFloat('Frangi', 'scale_range')
scale_step = IfNotFloat('Frangi', 'scale_step')
alpha = IfNotFloat('Frangi', 'alpha')
beta = IfNotFloat('Frangi', 'beta')
frangi_c = IfNotFloat('Frangi', 'frangi_c')
diff_mode = parameters['Frangi']['diff_mode']
bright_on_dark = parameters.getboolean('Frangi', 'bright_on_dark')
method = parameters['Frangi']['method']

# [Features Extraction]
hist_min_length = IfNotFloat('Features Extraction', 'hist_min_length')
hist_step_length = IfNotFloat('Features Extraction', 'hist_step_length')
number_of_bins_length = IfNotFloat('Features Extraction', 'number_of_bins_length')
hist_bins_borders_length = IfNotFloat('Features Extraction', 'hist_bins_borders_length')
hist_min_curvature = IfNotFloat('Features Extraction', 'hist_min_curvature')
hist_step_curvature = IfNotFloat('Features Extraction', 'hist_step_curvature')
number_of_bins_curvature = IfNotFloat('Features Extraction', 'number_of_bins_curvature')
hist_bins_borders_curvature = IfNotFloat('Features Extraction', 'hist_bins_borders_curvature')

# [Program running]
with_plot = parameters.getboolean('Program Running', 'with_plot')
in_parallel = parameters.getboolean('Program Running', 'in_parallel')

# data_path = "./data/DIO_6H_6_1.70bis_2.2_3.tif"
# channel = 'G'
# size_voxel_in_micron = None  # -> list [X,Y,Z]
# with_plot = False
#
# soma_low_c = 0.15
# soma_high_c = 0.7126
# ext_low_c = 0.2 #3 #1e-7    ##0.2  # 0.02  # 0.2  # ext_low_c = 9.0e-4
# ext_high_c = 0.6 #1e-10 #1.2e-7    ##0.6  # 0.04  # 0.6  # high_ext = 8.0e-3
#
# # soma_selem_c = mp_.disk(2)
# # ext_selem_c = mp_.disk(1)
# soma_selem_micron_c = 0.24050024 * 2
# ext_selem_micron_c = 0.24050024 * 1
#
# max_straight_sq_dist_c = (30 ** 2) * 0.24050024
# max_weighted_length_c = 20.0 * 0.24050024
#
# soma_min_area_c = 1000 * (0.24050024 ** 2)
# ext_min_area_c = 100 * (0.24050024 ** 2)
# # soma_min_area_c = 1000
# # ext_min_area_c = 100
#
# in_parallel = False
