<# : nutrimorph.bat
:: Inspired from: see below
:: address/from/source:
::     https://exceptionshub.com/file-folder-chooser-dialog-from-a-windows-batch-script.html
::     (Update 2016.3.20)
:: https://stackoverflow.com/a/15885133/1683264

@echo off
setlocal

where /q powershell
if ERRORLEVEL 1 (
    echo PowerShell utility is missing
    echo Please install PowerShell from Microsoft
    echo [Press "Enter" to exit]
    pause > nul
    exit
)

for /f "delims=" %%i in ('powershell -noprofile "iex (${%~f0} | out-string)"') do (
	.\nutrimorph\nutrimorph.exe "%%~i"
)
echo [Press "Enter" to exit]
pause > nul
goto :EOF

: end Batch portion / begin PowerShell hybrid chimera #>

Add-Type -AssemblyName System.Windows.Forms
$dialog = new-object Windows.Forms.OpenFileDialog
$dialog.InitialDirectory = pwd
$dialog.Filter = "PY Files (*.py)|*.py"
$dialog.ShowHelp = $true
$dialog.Multiselect = $false
[void]$dialog.ShowDialog()
$dialog.FileName
# if ($dialog.Multiselect) { $dialog.FileNames } else { $dialog.FileName }
